const {ethers} = require('ethers');
const web3 = require('web3');
const sdk = require('api')('@aevo/v1.0.3-beta#b186p82aalkustyah');
const currentProvider = new web3.providers.HttpProvider('https://mainnet.optimism.io');
const provider = new ethers.providers.Web3Provider(currentProvider);


const main = async () => {
    const privateKey = "YOUR_PK"
    const account = "YOUR_ACCOUNT"
    const collateral = 'YOUR_COLLATERAL'
    const amount = 1000000000000
    const salt = Math.floor(Math.random() * 100000)
    const to = "YOUR_TO_ADDR"


    const wallet = new ethers.Wallet(privateKey, provider);

    const domainData = {
        name: "Aevo Mainnet",
        version: "1",
        chainId: 1,
    }

    const withdrawType = [
        {name: "collateral", type: "address"},
        {name: "to", type: "address"},
        {name: "amount", type: "uint256"},
        {name: "salt", type: "uint256"},
        {name: "data", type: "bytes32"},
    ];

    const withdrawMessage = {
        collateral,
        to,
        amount: amount.toString(),
        salt: salt.toString(),
        data: ethers.utils.keccak256("0x"),
    };

    const socket_fees = "1179244658442942";
    const socket_msg_gas_limit = "500000";
    const socket_connector = "0x48B4f0692eaA84F1961b64342Ae746D40d9ac2F2"
    const encodedSocketData = ethers.utils.defaultAbiCoder.encode(
        ["uint256", "uint256", "address"],
        [socket_fees, socket_msg_gas_limit, socket_connector]
    );

    withdrawMessage.data = ethers.utils.keccak256(encodedSocketData);

    const withdrawSignature = await wallet._signTypedData(
        domainData,
        {Withdraw: withdrawType},
        withdrawMessage
    );

    const withdrawPayload = {
        account,
        signature: ethers.utils.joinSignature(
            ethers.utils.splitSignature(withdrawSignature)
        ),
        collateral: withdrawMessage.collateral,
        to: withdrawMessage.to,
        amount: withdrawMessage.amount,
        salt: withdrawMessage.salt,
        socket_fees: "1179244658442942",
        socket_msg_gas_limit: "500000",
        socket_connector: "0x48B4f0692eaA84F1961b64342Ae746D40d9ac2F2"
    };

    return await sdk.postWithdraw(withdrawPayload)

}

(async () => {
    try {
        const {data} = await main()
        console.log(data)
        console.log("withdraw successfully")
        process.exit(0)
    } catch (err) {
        console.error('withdraw:', err);
        process.exit(1)
    }
})();

